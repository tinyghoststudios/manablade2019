﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
	public class CharacterVFX : MonoBehaviour
	{
		public GameObject vfxSpawnLocation;
		public GameObject[] _vfxGameObjects;

		void Start()
		{
			SetReferences();
		}

		void SetReferences()
		{
			vfxSpawnLocation = GetComponentInChildren<TagVFXSpawnLocation>().gameObject;
		}

		
	}
}