﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using System;

namespace MoreMountains.CorgiEngine
{
	/// <summary>
	/// Add this class to a character and it'll be able to jump
	/// Animator parameters : Jumping (bool), DoubleJumping (bool), HitTheGround (bool)
	/// </summary>

	[AddComponentMenu("Corgi Engine/Character/Abilities/Character Mobility")]
	
	public class CharacterMobility : CharacterAbility
	{
		#region DEBUG INSPECTOR
		public CharacterStatus _status;
		#endregion

		#region INSPECTOR
		/// This method is only used to display a helpbox text at the beginning of the ability's inspector
		//public override string HelpBoxText() { return "Walking, Dashing, Jumping, Crouching"; }

		/// the possible jump restrictions
		public enum JumpBehavior
		{
			CanJumpOnGround,
			CanJumpOnGroundAndFromLadders,
			CanJumpAnywhere,
			CantJump,
			CanJumpAnywhereAnyNumberOfTimes
		}

		[Header("Jump Behaviour")]
		/// defines how high the character can jump
		public float JumpHeight = 2.025f;
		/// the maximum number of jumps allowed (0 : no jump, 1 : normal jump, 2 : double jump, etc...)
		public int NumberOfJumps = 3;
		/// basic rules for jumps : where can the player jump ?
		public JumpBehavior JumpRestrictions = JumpBehavior.CanJumpAnywhere;

		public float GetHorizontalInput()
		{
			return _horizontalInput;
		}

		/// a timeframe during which, after leaving the ground, the character can still trigger a jump
		public float JumpTimeWindow = 0f;
		/// if this is true, camera offset will be reset on jump
		public bool ResetCameraOffsetOnJump = false;

		//[Header("Proportional jumps")]
		/// if true, the jump duration/height will be proportional to the duration of the button's press
		[HideInInspector] public bool JumpIsProportionalToThePressTime = true;
		/// the minimum time in the air allowed when jumping - this is used for pressure controlled jumps
		[HideInInspector] public float JumpMinimumAirTime = 0.1f;
		/// the amount by which we'll modify the current speed when the jump button gets released
		[HideInInspector] public float JumpReleaseForceFactor = 2f;

		[Header("Collisions")]
		// duration (in seconds) we need to disable collisions when jumping down a 1 way platform
		[HideInInspector] public float OneWayPlatformsJumpCollisionOffDuration = 0.3f;
		// duration (in seconds) we need to disable collisions when jumping off a moving platform
		[HideInInspector] public float MovingPlatformsJumpCollisionOffDuration = 0.05f;


		[Header("Dash")]
		/// the duration of dash (in seconds)
		public float DashDistance = 1.5f;
		/// the force of the dash
		public float DashForce = 6f;
		/// the duration of the cooldown between 2 dashes (in seconds)
		[HideInInspector] public float DashCooldown = 1f;


		[Header("Speed")]
		/// the speed of the character when it's walking
		public float WalkSpeed = 6f;
		/// the multiplier to apply to the horizontal movement
		[ReadOnly]
		[HideInInspector] public float MovementSpeedMultiplier = 1f;
		[ReadOnly]
		[HideInInspector] public float PushSpeedMultiplier = 1f;
		/// the current horizontal movement force
		public float HorizontalMovementForce { get { return _horizontalMovementForce; } }
		/// if this is true, movement will be forbidden (as well as flip)
		public bool MovementForbidden { get; set; }

		[Header("Input")]
		/// if this is true, no acceleration will be applied to the movement, which will instantly be full speed (think Megaman movement)
		public bool InstantAcceleration = false;
		/// the threshold after which input is considered (usually 0.1f to eliminate small joystick noise)
		public float InputThreshold = 0.1f;

		[Header("Effects")]
		/// the effect that will be instantiated everytime the character touches the ground
		[HideInInspector] public ParticleSystem TouchTheGroundEffect;
		/// the sound effect to play when the character touches the ground
		[HideInInspector] public AudioClip TouchTheGroundSfx;

		public CharacterVFX _vfx;

		#endregion

		#region SHARED FUNCTIONS

		/// <summary>
		/// On Start() we reset our number of jumps
		/// </summary>
		protected override void Initialization()
		{
			base.Initialization();
			_status = GetComponent<CharacterStatus>();
			_vfx = GetComponent<CharacterVFX>();

			InitializationJump();
			InitializationHorizontalMovement();
			InitializationCrouch();
		}

		/// <summary>
		/// At the beginning of each cycle we check if we've just pressed or released the jump button
		/// </summary>
		protected override void HandleInput()
		{
			//HORIZONTAL INPUT
			WalkStart();

			//JUMP
			if (_inputManager.JumpButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)
				JumpStart();
			if (_inputManager.JumpButton.State.CurrentState == MMInput.ButtonStates.ButtonUp)
				JumpStop();

			//DASH
			if (_inputManager.ReloadButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)
				StartDash();

			//CROUCH
			if (_verticalInput < -_inputManager.Threshold.y)
				Crouch();
		}

		/// <summary>
		/// Every frame we perform a number of checks related to jump
		/// </summary>
		public override void ProcessAbility()
		{
			//Debug.Log(Time.frameCount + " START PROCESSING MOBILITY");
			base.ProcessAbility();
			//JUMP
			ProcessAbilityJump();

			//DASH
			ProcessAbilityDash();
			// If the character is dashing, we cancel the gravity
			
			//HORIZONTAL MOVEMENT
			ProcessHorizontalMovement();

			//CROUCH
			ProcessAbilityCrouch();

			
		}

		/// <summary>
		/// Adds required animator parameters to the animator parameters list if they exist
		/// </summary>
		protected override void InitializeAnimatorParameters()
		{

		}

		/// <summary>
		/// At the end of each cycle, sends Jumping states to the Character's animator
		/// </summary>
		public override void UpdateAnimator()
		{
		
		}

		// Check if input is saying the character should turn around
		public void CheckIfShouldFlip()
		{
			if (_horizontalInput > InputThreshold && !_character.IsFacingRight || _horizontalInput < -InputThreshold && _character.IsFacingRight)
				_character.Flip();
		}

		#endregion

		#region JUMP
		
		/// the number of jumps left to the character
		[ReadOnly]
		[HideInInspector] public int NumberOfJumpsLeft;
		/// whether or not the jump happened this frame
		public bool JumpHappenedThisFrame { get; set; }
		/// whether or not the jump can be stopped
		public bool CanJumpStop { get; set; }

		protected float _jumpButtonPressTime = 0;
		protected bool _jumpButtonPressed = false;
		protected bool _jumpButtonReleased = false;
		protected bool _doubleJumping = false;
		protected CharacterWalljump _characterWallJump = null;
		protected CharacterCrouch _characterCrouch = null;
		protected CharacterButtonActivation _characterButtonActivation = null;
		protected CharacterLadder _characterLadder = null;
		protected int _initialNumberOfJumps;

		protected float _lastTimeGrounded = 0f;

		/// Evaluates the jump restrictions
		public bool JumpAuthorized
		{
			get
			{
				if (EvaluateJumpTimeWindow())
				{
					return true;
				}

				if (_movement.CurrentState == CharacterStates.MovementStates.SwimmingIdle)
				{
					return false;
				}

				if ((JumpRestrictions == JumpBehavior.CanJumpAnywhere) || (JumpRestrictions == JumpBehavior.CanJumpAnywhereAnyNumberOfTimes))
				{
					return true;
				}

				if (JumpRestrictions == JumpBehavior.CanJumpOnGround)
				{
					if (_controller.State.IsGrounded
						|| (_movement.CurrentState == CharacterStates.MovementStates.Gripping)
						|| (_movement.CurrentState == CharacterStates.MovementStates.LedgeHanging))
					{
						return true;
					}
					else
					{
						// if we've already made a jump and that's the reason we're in the air, then yes we can jump
						if (NumberOfJumpsLeft < NumberOfJumps)
						{
							return true;
						}
					}
				}

				if (JumpRestrictions == JumpBehavior.CanJumpOnGroundAndFromLadders)
				{
					if ((_controller.State.IsGrounded)
						|| (_movement.CurrentState == CharacterStates.MovementStates.Gripping)
						|| (_movement.CurrentState == CharacterStates.MovementStates.LadderClimbing)
						|| (_movement.CurrentState == CharacterStates.MovementStates.LedgeHanging))
					{
						return true;
					}
					else
					{
						// if we've already made a jump and that's the reason we're in the air, then yes we can jump
						if (NumberOfJumpsLeft < NumberOfJumps)
						{
							return true;
						}
					}
				}

				return false;
			}
		}

		protected void InitializationJump()
		{
			ResetNumberOfJumps();
			_characterWallJump = GetComponent<CharacterWalljump>();
			_characterCrouch = GetComponent<CharacterCrouch>();
			_characterButtonActivation = GetComponent<CharacterButtonActivation>();
			_characterLadder = GetComponent<CharacterLadder>();
			_initialNumberOfJumps = NumberOfJumps;
			CanJumpStop = true;
		}

		protected void ProcessAbilityJump()
		{
			JumpHappenedThisFrame = false;

			if (!AbilityPermitted) { return; }

			// if we just got grounded, we reset our number of jumps
			if (_controller.State.JustGotGrounded)
			{
				NumberOfJumpsLeft = NumberOfJumps;
				_doubleJumping = false;
			}

			// we store the last timestamp at which the character was grounded
			if (_controller.State.IsGrounded)
			{
				_lastTimeGrounded = Time.time;
			}

			// If the user releases the jump button and the character is jumping up and enough time since the initial jump has passed, then we make it stop jumping by applying a force down.
			if ((_jumpButtonPressTime != 0)
				&& (Time.time - _jumpButtonPressTime >= JumpMinimumAirTime)
				&& (_controller.Speed.y > Mathf.Sqrt(Mathf.Abs(_controller.Parameters.Gravity)))
				&& (_jumpButtonReleased)
				&& (!_jumpButtonPressed
					|| (_movement.CurrentState == CharacterStates.MovementStates.Jetpacking)))
			{
				_jumpButtonReleased = false;
				if (JumpIsProportionalToThePressTime)
				{
					_jumpButtonPressTime = 0;
					if (JumpReleaseForceFactor == 0f)
					{
						_controller.SetVerticalForce(0f);
					}
					else
					{
						_controller.AddVerticalForce(-_controller.Speed.y / JumpReleaseForceFactor);
					}
				}
			}

			UpdateController();
		}

		/// <summary>
		/// Determines if whether or not a Character is still in its Jump Window (the delay during which, after falling off a cliff, a jump is still possible without requiring multiple jumps)
		/// </summary>
		/// <returns><c>true</c>, if jump time window was evaluated, <c>false</c> otherwise.</returns>
		protected virtual bool EvaluateJumpTimeWindow()
		{
			if (_movement.CurrentState == CharacterStates.MovementStates.Jumping
				|| _movement.CurrentState == CharacterStates.MovementStates.DoubleJumping
				|| _movement.CurrentState == CharacterStates.MovementStates.WallJumping)
			{
				return false;
			}

			if (Time.time - _lastTimeGrounded <= JumpTimeWindow)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Evaluates the jump conditions to determine whether or not a jump can occur
		/// </summary>
		/// <returns><c>true</c>, if jump conditions was evaluated, <c>false</c> otherwise.</returns>
		protected virtual bool EvaluateJumpConditions()
		{
			bool onAOneWayPlatform = (_controller.OneWayPlatformMask.Contains(_controller.StandingOn.layer)
				|| _controller.MovingOneWayPlatformMask.Contains(_controller.StandingOn.layer));

			if (!AbilityPermitted  // if the ability is not permitted
				|| !JumpAuthorized // if jumps are not authorized right now
				|| (!_controller.CanGoBackToOriginalSize() && !onAOneWayPlatform)
				|| ((_condition.CurrentState != CharacterStates.CharacterConditions.Normal) // or if we're not in the normal stance
					&& (_condition.CurrentState != CharacterStates.CharacterConditions.ControlledMovement))
				|| (_movement.CurrentState == CharacterStates.MovementStates.Jetpacking) // or if we're jetpacking
				//|| (_movement.CurrentState == CharacterStates.MovementStates.Dashing) // or if we're dashing
				|| (_movement.CurrentState == CharacterStates.MovementStates.Pushing) // or if we're pushing                
				|| ((_movement.CurrentState == CharacterStates.MovementStates.WallClinging) && (_characterWallJump != null)) // or if we're wallclinging and can walljump
				|| (_controller.State.IsCollidingAbove && !onAOneWayPlatform)) // or if we're colliding with the ceiling
			{
				return false;
			}

			// if we're in a button activated zone and can interact with it
			if (_characterButtonActivation != null)
			{
				if (_characterButtonActivation.AbilityPermitted
					&& _characterButtonActivation.PreventJumpWhenInZone
					&& _characterButtonActivation.InButtonActivatedZone
					&& !_characterButtonActivation.InButtonAutoActivatedZone)
				{
					Debug.Log("in button activation");
					return false;
				}
			}

			// if we're crouching and don't have enough space to stand we do nothing and exit
			if ((_movement.CurrentState == CharacterStates.MovementStates.Crouching) || (_movement.CurrentState == CharacterStates.MovementStates.Crawling))
			{
				if (_characterCrouch != null)
				{
					if (_characterCrouch.InATunnel && (_verticalInput >= -_inputManager.Threshold.y))
					{
						Debug.Log("crouching and no space");
						return false;
					}
				}
			}

			// if we're not grounded, not on a ladder, and don't have any jumps left, we do nothing and exit
			if ((!_controller.State.IsGrounded)
				&& !EvaluateJumpTimeWindow()
				&& (_movement.CurrentState != CharacterStates.MovementStates.LadderClimbing)
				&& (JumpRestrictions != JumpBehavior.CanJumpAnywhereAnyNumberOfTimes)
				&& (NumberOfJumpsLeft <= 0))
			{
				Debug.Log("airborne, and no jumps left");
				return false;
			}

			if (_controller.State.IsGrounded
				&& (NumberOfJumpsLeft <= 0))
			{
				Debug.Log("on ground, but no jumps");
				return false;
			}

			if (_inputManager != null)
			{
				// if the character is standing on a one way platform and is also pressing the down button,
				if (_verticalInput < -_inputManager.Threshold.y && _controller.State.IsGrounded)
				{
					if (JumpDownFromOneWayPlatform())
					{
						Debug.Log("on a 1way platform, and holding down");
						return false;
					}
				}

				// if the character is standing on a moving platform and not pressing the down button,
				if (_verticalInput >= -_inputManager.Threshold.y && _controller.State.IsGrounded)
				{
					JumpFromMovingPlatform();
				}
			}
			return true;
		}

		/// <summary>
		/// Causes the character to start jumping.
		/// </summary>
		public virtual void JumpStart()
		{
			if (!EvaluateJumpConditions())
				return;

			//Debug.Log("starting jump");
			CheckIfShouldFlip();

			// we reset our walking speed
			if ((_movement.CurrentState == CharacterStates.MovementStates.Crawling)
				|| (_movement.CurrentState == CharacterStates.MovementStates.Crouching)
				|| (_movement.CurrentState == CharacterStates.MovementStates.LadderClimbing))
			{
				ResetHorizontalSpeed();
			}

			if (_movement.CurrentState == CharacterStates.MovementStates.LadderClimbing)
			{
				_characterLadder.GetOffTheLadder();
			}

			_controller.ResetColliderSize();

			// if we're still here, the jump will happen
			// we set our current state to Jumping
			_movement.ChangeState(CharacterStates.MovementStates.Jumping);

			// we trigger a character event
			MMCharacterEvent.Trigger(_character, MMCharacterEventTypes.Jump);

			// we start our sounds
			PlayAbilityStartSfx();

			if (ResetCameraOffsetOnJump)
			{
				_sceneCamera.ResetLookUpDown();
			}

			if (NumberOfJumpsLeft != NumberOfJumps)
			{
				_doubleJumping = true;
			}

			// we decrease the number of jumps left
			NumberOfJumpsLeft = NumberOfJumpsLeft - 1;

			// we reset our current condition and gravity
			_condition.ChangeState(CharacterStates.CharacterConditions.Normal);
			_controller.GravityActive(true);
			_controller.CollisionsOn();

			// we set our various jump flags and counters
			SetJumpFlags();
			CanJumpStop = true;

			// we make the character jump
			_controller.SetVerticalForce(Mathf.Sqrt(2f * JumpHeight * Mathf.Abs(_controller.Parameters.Gravity)));
			JumpHappenedThisFrame = true;
			
		}

		/// <summary>
		/// Handles jumping down from a one way platform.
		/// </summary>
		protected virtual bool JumpDownFromOneWayPlatform()
		{
			if (_controller.OneWayPlatformMask.Contains(_controller.StandingOn.layer)
				|| _controller.MovingOneWayPlatformMask.Contains(_controller.StandingOn.layer))
			{
				// we make it fall down below the platform by moving it just below the platform
				_controller.transform.position = new Vector2(transform.position.x, transform.position.y - 0.1f);
				// we turn the boxcollider off for a few milliseconds, so the character doesn't get stuck mid platform
				StartCoroutine(_controller.DisableCollisionsWithOneWayPlatforms(OneWayPlatformsJumpCollisionOffDuration));
				_controller.DetachFromMovingPlatform();
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Handles jumping from a moving platform.
		/// </summary>
		protected virtual void JumpFromMovingPlatform()
		{
			if (_controller.MovingPlatformMask.Contains(_controller.StandingOn.layer)
				|| _controller.MovingOneWayPlatformMask.Contains(_controller.StandingOn.layer))
			{
				// we turn the boxcollider off for a few milliseconds, so the character doesn't get stuck mid air
				StartCoroutine(_controller.DisableCollisionsWithMovingPlatforms(MovingPlatformsJumpCollisionOffDuration));
				_controller.DetachFromMovingPlatform();
			}
		}

		/// <summary>
		/// Causes the character to stop jumping.
		/// </summary>
		public virtual void JumpStop()
		{
			if (!CanJumpStop)
			{
				return;
			}
			_jumpButtonPressed = false;
			_jumpButtonReleased = true;
		}

		/// <summary>
		/// Resets the number of jumps.
		/// </summary>
		public virtual void ResetNumberOfJumps()
		{
			NumberOfJumpsLeft = NumberOfJumps;
		}

		/// <summary>
		/// Resets jump flags
		/// </summary>
		public virtual void SetJumpFlags()
		{
			_jumpButtonPressTime = Time.time;
			_jumpButtonPressed = true;
			_jumpButtonReleased = false;
		}

		/// <summary>
		/// Updates the controller state based on our current jumping state
		/// </summary>
		protected virtual void UpdateController()
		{
			_controller.State.IsJumping = (_movement.CurrentState == CharacterStates.MovementStates.Jumping
					|| _movement.CurrentState == CharacterStates.MovementStates.DoubleJumping
					|| _movement.CurrentState == CharacterStates.MovementStates.WallJumping);
		}

		/// <summary>
		/// Sets the number of jumps left.
		/// </summary>
		/// <param name="newNumberOfJumps">New number of jumps.</param>
		public virtual void SetNumberOfJumpsLeft(int newNumberOfJumps)
		{
			NumberOfJumpsLeft = newNumberOfJumps;
		}

		/// <summary>
		/// Resets the jump button released flag.
		/// </summary>
		public virtual void ResetJumpButtonReleased()
		{
			_jumpButtonReleased = false;
		}

		

		/// <summary>
		/// Resets parameters in anticipation for the Character's respawn.
		/// </summary>
		public override void Reset()
		{
			base.Reset();
			NumberOfJumps = _initialNumberOfJumps;
			NumberOfJumpsLeft = _initialNumberOfJumps;
		}

		#endregion
		#region DASH
		
		protected float _cooldownTimeStamp = 0;

		protected float _startTime;
		protected Vector3 _initialPosition;
		protected float _dashDirection;
		protected float _distanceTraveled = 0f;
		protected bool _shouldKeepDashing = true;
		protected float _computedDashForce;
		protected float _slopeAngleSave = 0f;
		protected bool _dashEndedNaturally = true;
		protected IEnumerator _dashCoroutine;

		public void ProcessAbilityDash()
		{
			if (_movement.CurrentState == CharacterStates.MovementStates.Dashing)
			{
				_controller.GravityActive(false);
				_controller.SetVerticalForce(0);
			}
			// we reset our slope tolerance if dash didn't end naturally
			if ((!_dashEndedNaturally) && (_movement.CurrentState != CharacterStates.MovementStates.Dashing))
			{
				_dashEndedNaturally = true;
				_controller.Parameters.MaximumSlopeAngle = _slopeAngleSave;
			}
		}

		/// <summary>
		/// Causes the character to dash or dive (depending on the vertical movement at the start of the dash)
		/// </summary>
		public virtual void StartDash()
		{
			// if the Dash action is enabled in the permissions, we continue, if not we do nothing
			if (!AbilityPermitted
				|| (_condition.CurrentState != CharacterStates.CharacterConditions.Normal)
				|| (_movement.CurrentState == CharacterStates.MovementStates.LedgeHanging)
				|| (_movement.CurrentState == CharacterStates.MovementStates.Gripping))
				return;

			// If the user presses the dash button and is not aiming down
			if (_verticalInput > -_inputManager.Threshold.y)
			{
				if (_status.canDefend)
				// if the character is allowed to dash
				//if (_cooldownTimeStamp <= Time.time)
				//{
					InitiateDash();
				//}
			}
		}

		public virtual void InitiateDash()
		{
			//holding opposite direction?
			CheckIfShouldFlip();

			// we set its dashing state to true
			_movement.ChangeState(CharacterStates.MovementStates.Dashing);
			_status.StartAction(ActionType.Dash);

			//_vfx.PlayVFX();

			// we start our sounds
			PlayAbilityStartSfx();
			PlayAbilityUsedSfx();

			//INSTEAD OF COOLDOWN, USE THE ACTION STEP SYSTEM
			//_cooldownTimeStamp = Time.time + DashCooldown;

			// we launch the boost corountine with the right parameters
			_dashCoroutine = Dash();
			StartCoroutine(_dashCoroutine);
		}


		/// <summary>
		/// Coroutine used to move the player in a direction over time
		/// </summary>
		protected virtual IEnumerator Dash()
		{
			// if the character is not in a position where it can move freely, we do nothing.
			if (!AbilityPermitted
				|| (_condition.CurrentState != CharacterStates.CharacterConditions.Normal))
			{
				yield break;
			}

			// we initialize our various counters and checks
			_startTime = Time.time;
			_dashEndedNaturally = false;
			_initialPosition = this.transform.position;
			_distanceTraveled = 0;
			_shouldKeepDashing = true;
			_dashDirection = _character.IsFacingRight ? 1f : -1f;
			_computedDashForce = DashForce * _dashDirection;

			// we prevent our character from going through slopes
			_slopeAngleSave = _controller.Parameters.MaximumSlopeAngle;
			_controller.Parameters.MaximumSlopeAngle = 0;

			// we keep dashing until we've reached our target distance or until we get interrupted
			while (_distanceTraveled < DashDistance && 
				_shouldKeepDashing && 
				//_movement.CurrentState == CharacterStates.MovementStates.Dashing
				_status.currentAction == ActionType.Dash
				)
			{
				_distanceTraveled = Vector3.Distance(_initialPosition, this.transform.position);

				// if we collide with something on our left or right (wall, slope), 
				//we stop dashing, otherwise we apply horizontal force
				if ((_controller.State.IsCollidingLeft || _controller.State.IsCollidingRight))
				{
					_shouldKeepDashing = false;
					_controller.SetForce(Vector2.zero);
				}
				else
				{
					_controller.GravityActive(false);
					_controller.SetVerticalForce(0);
					//Debug.Log("dash force " + _computedDashForce);
					_controller.SetHorizontalForce(_computedDashForce);
				}
				yield return null;
			}

			StopDash();
		}

		public virtual void StopDash()
		{
			StopCoroutine(_dashCoroutine);

			// once our dash is complete, we reset our various states

			_controller.DefaultParameters.MaximumSlopeAngle = _slopeAngleSave;
			_controller.Parameters.MaximumSlopeAngle = _slopeAngleSave;
			_controller.GravityActive(true);
			_dashEndedNaturally = true;

			// we play our exit sound
			StopAbilityUsedSfx();
			PlayAbilityStopSfx();

			// once the boost is complete, if we were dashing, we make it stop and start the dash cooldown
			if (_movement.CurrentState == CharacterStates.MovementStates.Dashing)
			{
				_movement.RestorePreviousState();
			}
		}

		#endregion
		#region HORIZONTAL MOVEMENT

		/// the current reference movement speed
		public float MovementSpeed { get; set; }

		[HideInInspector] public float _horizontalMovement;
		[HideInInspector] public float _horizontalMovementForce;
		[HideInInspector] public float _normalizedHorizontalSpeed;

		/// <summary>
		/// On Initialization, we set our movement speed to WalkSpeed.
		/// </summary>
		protected void InitializationHorizontalMovement()
		{
			MovementSpeed = WalkSpeed;
			MovementSpeedMultiplier = 1f;
			MovementForbidden = false;
		}

		/// <summary>
		/// Sets the horizontal move value.
		/// </summary>
		/// <param name="value">Horizontal move value, between -1 and 1 - positive : will move to the right, negative : will move left </param>
		public virtual void SetHorizontalMove(float value)
		{
			_horizontalMovement = value;
		}

		protected void WalkStart()
		{
			_horizontalMovement = _horizontalInput;
		}

		/// <summary>
		/// Called at Update(), handles horizontal movement
		/// </summary>
		protected virtual void ProcessHorizontalMovement()
		{
			//Debug.Log(Time.frameCount + " state " + _movement.CurrentState);

			// if we're not walking anymore, we stop our walking sound
			if (_movement.CurrentState != CharacterStates.MovementStates.Walking && _abilityInProgressSfx != null)
				StopAbilityUsedSfx();

			if (_movement.CurrentState == CharacterStates.MovementStates.Walking && _abilityInProgressSfx == null)
				PlayAbilityUsedSfx();
			
			// if movement is prevented, or if the character is dead/frozen/can't move, we exit and do nothing
			if (!AbilityPermitted
				|| (_condition.CurrentState != CharacterStates.CharacterConditions.Normal)
				|| (_movement.CurrentState == CharacterStates.MovementStates.Gripping))
			{
				return;
			}

			
			// check if we just got grounded
			CheckJustGotGrounded();

			if (MovementForbidden)
				_horizontalMovement = _controller.Speed.x * Time.deltaTime;

			if (!_status.canMove)
				_horizontalMovement = _controller.Speed.x * Time.deltaTime;

			// If the value of the horizontal axis is positive, the character must face right.
			if (_horizontalMovement > InputThreshold)
			{
				_normalizedHorizontalSpeed = _horizontalMovement;
				if (!_character.IsFacingRight)
				{
					if (_controller.State.IsGrounded)
						//if (_movement.CurrentState == CharacterStates.MovementStates.Idle ||
						//	_movement.CurrentState == CharacterStates.MovementStates.Walking)
						if (_status.currentAction == ActionType.Neutral || _status.currentAction == ActionType.Walk)
						{
							_character.Flip();
						}
				}
			}
			// If it's negative, then we're facing left
			else if (_horizontalMovement < -InputThreshold)
			{
				_normalizedHorizontalSpeed = _horizontalMovement;
				if (_character.IsFacingRight)
				{
					if (_controller.State.IsGrounded)
						//if (_movement.CurrentState == CharacterStates.MovementStates.Idle ||
						//	_movement.CurrentState == CharacterStates.MovementStates.Walking)
						if (_status.currentAction == ActionType.Neutral || _status.currentAction == ActionType.Walk)
							_character.Flip();
				}
			}
			else
			{
				_normalizedHorizontalSpeed = 0;
			}

			// if we're grounded and moving, and currently Idle, Running or Dangling, we become Walking
			if ((_controller.State.IsGrounded)
				&& (_normalizedHorizontalSpeed != 0)
				&& _status.canMove
				&& _status.currentAction == ActionType.Neutral
				&& ((_movement.CurrentState == CharacterStates.MovementStates.Idle)
					|| (_movement.CurrentState == CharacterStates.MovementStates.Dangling)))
			{
				_status.StartAction(ActionType.Walk);
				_movement.ChangeState(CharacterStates.MovementStates.Walking);
				PlayAbilityStartSfx();
				PlayAbilityUsedSfx();
			}

			// if we're walking and not moving anymore, we go back to the Idle state
			if ((_movement.CurrentState == CharacterStates.MovementStates.Walking)
				&& (_normalizedHorizontalSpeed == 0))
			{
				_movement.ChangeState(CharacterStates.MovementStates.Idle);
				
				PlayAbilityStopSfx();
			}

			if (_status.currentAction == ActionType.Walk && _normalizedHorizontalSpeed == 0)
			{
				_status.StartAction(ActionType.Neutral);
			}

			// if the character is not grounded, but currently idle or walking, we change its state to Falling
			if (!_controller.State.IsGrounded
				&& (
					(_movement.CurrentState == CharacterStates.MovementStates.Walking)
						|| (_movement.CurrentState == CharacterStates.MovementStates.Idle)
					))
			{
				_movement.ChangeState(CharacterStates.MovementStates.Falling);
			}

			if (InstantAcceleration)
			{
				if (_normalizedHorizontalSpeed > 0f) { _normalizedHorizontalSpeed = 1f; }
				if (_normalizedHorizontalSpeed < 0f) { _normalizedHorizontalSpeed = -1f; }
			}

			// we pass the horizontal force that needs to be applied to the controller.
			float movementFactor = _controller.State.IsGrounded ? _controller.Parameters.SpeedAccelerationOnGround : _controller.Parameters.SpeedAccelerationInAir;
			float movementSpeed =
				_normalizedHorizontalSpeed *
				MovementSpeed *
				_controller.Parameters.SpeedFactor *
				MovementSpeedMultiplier *
				PushSpeedMultiplier;

			if (!InstantAcceleration)
			{
				_horizontalMovementForce = Mathf.Lerp(_controller.Speed.x, movementSpeed, Time.deltaTime * movementFactor);
			}
			else
			{
				_horizontalMovementForce = movementSpeed;
			}



			// we handle friction
			_horizontalMovementForce = HandleFriction(_horizontalMovementForce);

			// we set our newly computed speed to the controller
			_controller.SetHorizontalForce(_horizontalMovementForce);
		}

		/// <summary>
		/// Every frame, checks if we just hit the ground, and if yes, changes the state and triggers a particle effect
		/// </summary>
		protected virtual void CheckJustGotGrounded()
		{
			// if the character just got grounded
			if (_controller.State.JustGotGrounded)
			{
				if (_controller.State.ColliderResized)
				{
					_movement.ChangeState(CharacterStates.MovementStates.Crouching);
				}
				else
				{
					//Debug.Log("landed and idle");
					_movement.ChangeState(CharacterStates.MovementStates.Idle);
				}

				_controller.SlowFall(0f);
				if (TouchTheGroundEffect != null)
				{
					Instantiate(TouchTheGroundEffect, _controller.BoundsBottom, transform.rotation);
				}
				PlayTouchTheGroundSfx();
			}
		}

		/// <summary>
		/// Handles surface friction.
		/// </summary>
		/// <returns>The modified current force.</returns>
		/// <param name="force">the force we want to apply friction to.</param>
		protected virtual float HandleFriction(float force)
		{
			// if we have a friction above 1 (mud, water, stuff like that), we divide our speed by that friction
			if (_controller.Friction > 1)
			{
				force = force / _controller.Friction;
			}

			// if we have a low friction (ice, marbles...) we lerp the speed accordingly
			if (_controller.Friction < 1 && _controller.Friction > 0)
			{
				force = Mathf.Lerp(_controller.Speed.x, force, Time.deltaTime * _controller.Friction * 10);
			}

			return force;
		}

		/// <summary>
		/// Plays the touch the ground sfx. Triggered when hitting the ground from any state
		/// </summary>
		protected virtual void PlayTouchTheGroundSfx()
		{
			if (TouchTheGroundSfx != null) { SoundManager.Instance.PlaySound(TouchTheGroundSfx, transform.position); }
		}


		/// <summary>
		/// A public method to reset the horizontal speed
		/// </summary>
		public virtual void ResetHorizontalSpeed()
		{
			MovementSpeed = WalkSpeed;
		}


		protected virtual void OnRevive()
		{
			Initialization();
		}

		/// <summary>
		/// When the player respawns, we reinstate this agent.
		/// </summary>
		/// <param name="checkpoint">Checkpoint.</param>
		/// <param name="player">Player.</param>
		protected override void OnEnable()
		{
			base.OnEnable();
			if (gameObject.GetComponentNoAlloc<Health>() != null)
			{
				gameObject.GetComponentNoAlloc<Health>().OnRevive += OnRevive;
			}
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			if (_health != null)
			{
				_health.OnRevive -= OnRevive;
			}
		}

		#endregion
		#region CROUCH

		[Header("Crawl")]
		/// if this is set to false, the character won't be able to crawl, just to crouch
		public bool CrawlAuthorized = true;
		/// the speed of the character when it's crouching
		public float CrawlSpeed = 4f;

		[Space(10)]
		[Header("Crouching")]
		/// if this is true, the collider will be resized when crouched
		public bool ResizeColliderWhenCrouched = false;
		/// the size to apply to the collider when crouched (if ResizeColliderWhenCrouched is true, otherwise this will be ignored)
		public Vector2 CrouchedBoxColliderSize = new Vector2(1, 1);
		/// if this is true, the character is crouched and has an obstacle over its head that prevents it from getting back up again
		[ReadOnly]
		public bool InATunnel;

		/// <summary>
		/// On Start(), we set our tunnel flag to false
		/// </summary>
		protected void InitializationCrouch()
		{
			InATunnel = false;
		}

		/// <summary>
		/// Every frame, we check if we're crouched and if we still should be
		/// </summary>
		public void ProcessAbilityCrouch()
		{
			DetermineState();
			CheckExitCrouch();
		}


		/// <summary>
		/// If we're pressing down, we check if we can crouch or crawl, and change states accordingly
		/// </summary>
		protected virtual void Crouch()
		{
			if (!AbilityPermitted // if the ability is not permitted
				|| (_condition.CurrentState != CharacterStates.CharacterConditions.Normal) // or if we're not in our normal stance
				|| (!_controller.State.IsGrounded) // or if we're grounded
				|| (_movement.CurrentState == CharacterStates.MovementStates.Gripping) // or if we're gripping
				|| (_movement.CurrentState == CharacterStates.MovementStates.Jumping)) // OR IF WE'RE JUMPING
			{
				// we do nothing and exit
				return;
			}

			// if this is the first time we're here, we trigger our sounds
			if ((_movement.CurrentState != CharacterStates.MovementStates.Crouching) && (_movement.CurrentState != CharacterStates.MovementStates.Crawling))
			{
				// we play the crouch start sound 
				PlayAbilityStartSfx();
				PlayAbilityUsedSfx();
			}

			// we set the character's state to Crouching and if it's also moving we set it to Crawling
			_movement.ChangeState(CharacterStates.MovementStates.Crouching);
			if ((Mathf.Abs(_horizontalInput) > 0) && (CrawlAuthorized))
			{
				_movement.ChangeState(CharacterStates.MovementStates.Crawling);
			}

			// we resize our collider to match the new shape of our character (it's usually smaller when crouched)
			if (ResizeColliderWhenCrouched)
			{
				_controller.ResizeCollider(CrouchedBoxColliderSize);
				Invoke("RecalculateRays", Time.deltaTime * 10);
			}

			// we change our character's speed
			MovementSpeed = CrawlSpeed;

			// we prevent movement if we can't crawl
			if (!CrawlAuthorized)
			{
				MovementSpeed = 0f;
			}

			// we make our camera look down
			if (_sceneCamera != null)
			{
				_sceneCamera.LookDown();
			}
		}

		/// <summary>
		/// Runs every frame to check if we should switch from crouching to crawling or the other way around
		/// </summary>
		protected virtual void DetermineState()
		{
			if ((_movement.CurrentState == CharacterStates.MovementStates.Crouching) 
				|| (_movement.CurrentState == CharacterStates.MovementStates.Crawling))
			{
				if ((Mathf.Abs(_horizontalInput) > 0) && (CrawlAuthorized))
				{
					_movement.ChangeState(CharacterStates.MovementStates.Crawling);
				}
				else
				{
					_movement.ChangeState(CharacterStates.MovementStates.Crouching);
				}
			}
		}

		/// <summary>
		/// Every frame, we check to see if we should exit the Crouching (or Crawling) state
		/// </summary>
		protected virtual void CheckExitCrouch()
		{
			if (_inputManager == null)
			{
				ExitCrouch();
			}

			// if we're currently grounded
			if ((_movement.CurrentState == CharacterStates.MovementStates.Crouching)
				|| (_movement.CurrentState == CharacterStates.MovementStates.Crawling))
			{
				// but we're not pressing down anymore, or we're not grounded anymore
				if ((!_controller.State.IsGrounded) || (_verticalInput >= -_inputManager.Threshold.y))
				{
					ExitCrouch();
				}
			}
		}

		/// <summary>
		/// Exits the crouched state
		/// </summary>
		protected virtual void ExitCrouch()
		{
			// we cast a raycast above to see if we have room enough to go back to normal size
			InATunnel = !_controller.CanGoBackToOriginalSize();

			// if the character is not in a tunnel, we can go back to normal size
			if (!InATunnel)
			{
				// we return to normal walking speed
				ResetHorizontalSpeed();
				
				if (_sceneCamera != null)
				{
					_sceneCamera.ResetLookUpDown();
				}

				// we play our exit sound
				StopAbilityUsedSfx();
				PlayAbilityStopSfx();

				// we go back to Idle state and reset our collider's size
				_movement.ChangeState(CharacterStates.MovementStates.Idle);
				_controller.ResetColliderSize();
				Invoke("RecalculateRays", Time.deltaTime * 10);
			}
		}


		/// <summary>
		/// Recalculates the raycast's origin points.
		/// </summary>
		protected virtual void RecalculateRays()
		{
			_character.RecalculateRays();
		}

		#endregion
	}
}