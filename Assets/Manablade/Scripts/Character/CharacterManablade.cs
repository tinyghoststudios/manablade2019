﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using System.Collections.Generic;

namespace MoreMountains.CorgiEngine
{
	public class CharacterManablade : Character
	{
		public CharacterStatus _steps;
		//public CorgiControllerMB _controllerMB;

		public ActionType currentAction;
		protected GameObject internalGO;
		protected GameObject rootGO;

		protected override void Initialization()
		{
			//FIND VISUAL CONTAINER FOR FLIPPING
			internalGO = GetComponentInChildren<TagInternalGameObject>().gameObject;
			//internalGO = FindObjectOfType<TagInternalGameObject>().gameObject;
			rootGO = gameObject.transform.root.gameObject;
			base.Initialization();
			_controller = GetComponent<CorgiControllerMB>();
		}

		//Animator is on child, not root obj
		public override void AssignAnimator()
		{
			if (CharacterAnimator != null)
			{
				_animator = CharacterAnimator;
			}
			else
			{
				_animator = GetComponentInChildren<Animator>();
			}

			if (_animator != null)
			{
				InitializeAnimatorParameters();
			}
		}

		//Modified to flip the Visual sprite container instead of a sprite renderer
		public override void Flip(bool IgnoreFlipOnDirectionChange = false)
		{
			// if we don't want the character to flip, we do nothing and exit
			if (!FlipModelOnDirectionChange && !RotateModelOnDirectionChange && !IgnoreFlipOnDirectionChange)
			{
				return;
			}

			if (!CanFlip)
			{
				return;
			}
			
			if (!FlipModelOnDirectionChange && !RotateModelOnDirectionChange && IgnoreFlipOnDirectionChange)
			{
				if (CharacterModel != null)
				{
					CharacterModel.transform.localScale = Vector3.Scale(CharacterModel.transform.localScale, ModelFlipValue);
				}
				else
				{
					// if we're sprite renderer based, we revert the flipX attribute
					if (_spriteRenderer != null)
					{
						_spriteRenderer.flipX = !_spriteRenderer.flipX;
					}
				}
			}

			// Flips the character horizontally
			FlipModel();

			//FLIP THE VISUAL CONTAINER
			if (internalGO != null)
			{
				Vector3 newScale = internalGO.transform.localScale;
				newScale.x *= -1;
				internalGO.transform.localScale = newScale;
			}

			IsFacingRight = !IsFacingRight;

			// we tell all our abilities we should flip
			foreach (CharacterAbility ability in _characterAbilities)
				if (ability.enabled) ability.Flip();
		}
	}
}