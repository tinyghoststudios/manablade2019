﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
	public class CharacterAttacks : CharacterAbility
	{
		public CharacterMobility _mobility;
		public CharacterStatus _status;
		public CharacterVFX _vfx;
		public DatabaseAction database;
		public Rigidbody2D rb;

		//SwordMitsuru5B
		#region SHARED FUNCTIONS
		protected override void Initialization()
		{
			base.Initialization();
			rb = GetComponent<Rigidbody2D>();
			_status = GetComponent<CharacterStatus>();
			_vfx = GetComponent<CharacterVFX>();
			_mobility = GetComponent<CharacterMobility>();
			database = DatabaseAction.Instance;
		}

		/// <summary>
		/// At the beginning of each cycle we check if we've just pressed or released the jump button
		/// </summary>
		protected override void HandleInput()
		{
			if (_inputManager.DashButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)
			{
				if (_horizontalInput != 0)
					StartAction(ActionType.swordMitsuruDroit);
				else
					//StartSwordMitsuru5B();
					StartAction(ActionType.SwordMitsuru5B);
			}
				
		}

		/// <summary>
		/// Every frame we perform a number of checks related to jump
		/// </summary>
		public override void ProcessAbility()
		{
			base.ProcessAbility();

			//DASH
			ProcessAbilitySwordMitsuru5B();
		}

		#endregion

		#region ATTACKS
		public void InitiateSwordMitsuru5B()
		{
			_mobility.CheckIfShouldFlip();
			_status.StartAction(ActionType.SwordMitsuru5B);
			_animator.Play("Sword Mitsuru 5B", -1, 0);
		}
		public void StartAction(ActionType act)
		{
			if (!AbilityPermitted
				|| (_condition.CurrentState != CharacterStates.CharacterConditions.Normal)
				|| (_movement.CurrentState == CharacterStates.MovementStates.LedgeHanging)
				|| (_movement.CurrentState == CharacterStates.MovementStates.Gripping))
				return;

			if (_status.canAttack)
			{
				InitiateAttack(act);
			}
		}
		public void InitiateAttack(ActionType act)
		{
			_mobility.CheckIfShouldFlip();
			_status.StartAction(act);
			_animator.Play(database.GetAnimationName(act), -1, 0);
		}
		public void StartSwordMitsuru5B()
		{
			if (_status.canAttack)
			{
				InitiateSwordMitsuru5B();
			}
		}
		public void ProcessAbilitySwordMitsuru5B()
		{
			if (_status.currentAction == ActionType.SwordMitsuru5B)
			{
				//_controller.GravityActive(false);
				//_controller.SetVerticalForce(0);
			}
		}
		#endregion

		#region COLLISIONS
		public void HandleCollisions(Collider2D collision)
		{
			
		}
		#endregion
	}
}
