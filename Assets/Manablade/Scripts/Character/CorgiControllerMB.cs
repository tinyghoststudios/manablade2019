﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
	public class CorgiControllerMB : CorgiController
	{
		public Vector2 _rootMotionViaAnimation;
		protected GameObject internalGO;

		//init rootmotion vector
		protected override void Initialization()
		{
			base.Initialization();
			_rootMotionViaAnimation = Vector2.zero;
			internalGO = GetComponentInChildren<TagInternalGameObject>().gameObject;
		}

		//reset rootmotion vector
		protected override void EveryFrame()
		{
			base.EveryFrame();
			_rootMotionViaAnimation = Vector2.zero;
		}

		//call reposition go for root motion
		protected override void FrameInitialization()
		{
			_contactList.Clear();
			RepositionInternalGO();
			// we initialize our newposition, which we'll use in all the next computations			
			_newPosition = Speed * Time.deltaTime + _rootMotionViaAnimation;
			State.WasGroundedLastFrame = State.IsCollidingBelow;
			StandingOnLastFrame = StandingOn;
			State.WasTouchingTheCeilingLastFrame = State.IsCollidingAbove;
			CurrentWallCollider = null;
			State.Reset();
		}

		//account for rot motion when determining move direction
		protected override void DetermineMovementDirection()
		{
			_movementDirection = _storedMovementDirection;
			if ((_speed.x + _rootMotionViaAnimation.x < -_movementDirectionThreshold) || 
				(_externalForce.x < -_movementDirectionThreshold))
			{
				_movementDirection = -1;
			}
			else if ((_speed.x + _rootMotionViaAnimation.x > _movementDirectionThreshold) || 
				(_externalForce.x > _movementDirectionThreshold))
			{
				_movementDirection = 1;
			}

			if (_movingPlatform != null)
			{
				if (Mathf.Abs(_movingPlatform.CurrentSpeed.x) > Mathf.Abs(_speed.x))
				{
					_movementDirection = Mathf.Sign(_movingPlatform.CurrentSpeed.x);
				}
			}

			//Debug.Log(Time.frameCount + " moving direction set to " + _movementDirection);
			_storedMovementDirection = _movementDirection;
		}

		//horizontalRayLength impacted by root motion vector
		protected override void CastRaysToTheSides(float raysDirection)
		{
			
			// we determine the origin of our rays
			_horizontalRayCastFromBottom = (_boundsBottomRightCorner + _boundsBottomLeftCorner) / 2;
			_horizontalRayCastToTop = (_boundsTopLeftCorner + _boundsTopRightCorner) / 2;
			_horizontalRayCastFromBottom = _horizontalRayCastFromBottom + (Vector2)transform.up * _obstacleHeightTolerance;
			_horizontalRayCastToTop = _horizontalRayCastToTop - (Vector2)transform.up * _obstacleHeightTolerance;

			// we determine the length of our rays
			float horizontalRayLength = 
				Mathf.Abs(_speed.x * Time.deltaTime + _rootMotionViaAnimation.x) + _boundsWidth / 2 + RayOffset * 2;

			//Debug.Log("");
			//Debug.Log(Time.frameCount);
			//Debug.Log("direction   " + raysDirection);
			//Debug.Log("speed * dt  " + (_speed.x * Time.deltaTime));
			//Debug.Log("root motion " + (_rootMotionViaAnimation.x));
			//Debug.Log("offset      " + (_boundsWidth / 2 + RayOffset * 2));
			//Debug.Log("raylength   " + horizontalRayLength);
			
			// we resize our storage if needed
			if (_sideHitsStorage.Length != NumberOfHorizontalRays)
				_sideHitsStorage = new RaycastHit2D[NumberOfHorizontalRays];

			// we cast rays to the sides
			for (int i = 0; i < NumberOfHorizontalRays; i++)
			{
				Vector2 rayOriginPoint = Vector2.Lerp(
					_horizontalRayCastFromBottom,
					_horizontalRayCastToTop,
					(float)i / (float)(NumberOfHorizontalRays - 1));

				// if we were grounded last frame and if this is our first ray, we don't cast against one way platforms
				if (State.WasGroundedLastFrame && i == 0)
					_sideHitsStorage[i] = MMDebug.RayCast(rayOriginPoint, raysDirection * (transform.right), horizontalRayLength, PlatformMask, MoreMountains.Tools.Colors.Indigo, Parameters.DrawRaycastsGizmos);
				else
					_sideHitsStorage[i] = MMDebug.RayCast(rayOriginPoint, raysDirection * (transform.right), horizontalRayLength, PlatformMask & ~OneWayPlatformMask & ~MovingOneWayPlatformMask, MoreMountains.Tools.Colors.Indigo, Parameters.DrawRaycastsGizmos);

				// if we've hit something
				if (_sideHitsStorage[i].distance > 0)
				{
					// if this collider is on our ignore list, we break
					if (_sideHitsStorage[i].collider == _ignoredCollider)
						break;

					//Debug.Log("HIT");
					//Debug.Log(Time.frameCount + " hit something " + _sideHitsStorage[i].distance);
					
					
					// we determine and store our current lateral slope angle
					float hitAngle = Mathf.Abs(Vector2.Angle(_sideHitsStorage[i].normal, transform.up));

					// we check if this is our movement direction
					if (_movementDirection == raysDirection)
						State.LateralSlopeAngle = hitAngle;

					// if the lateral slope angle is higher than our maximum slope angle, 
					// then we've hit a wall, and stop x movement accordingly
					if (hitAngle > Parameters.MaximumSlopeAngle)
					{
						//Debug.Log("hit angle " + hitAngle);

						if (raysDirection < 0)
						{
							State.IsCollidingLeft = true;
							State.DistanceToLeftCollider = _sideHitsStorage[i].distance;
						}
						else
						{
							State.IsCollidingRight = true;
							State.DistanceToRightCollider = _sideHitsStorage[i].distance;
						}

						//Debug.Log("distance " + MMMaths.DistanceBetweenPointAndLine(
						//		_sideHitsStorage[i].point,
						//		_horizontalRayCastFromBottom, _horizontalRayCastToTop));

						if (_movementDirection == raysDirection)
						{
							CurrentWallCollider = _sideHitsStorage[i].collider.gameObject;
							State.SlopeAngleOK = false;

							float distance = MMMaths.DistanceBetweenPointAndLine(
								_sideHitsStorage[i].point, 
								_horizontalRayCastFromBottom, _horizontalRayCastToTop);
							
							if (raysDirection <= 0)
								_newPosition.x = -distance
									+ _boundsWidth / 2 + RayOffset * 2;
							else
								_newPosition.x = distance
									- _boundsWidth / 2 - RayOffset * 2;

							//Debug.Log("newposition.x now set to " + _newPosition.x);

							_rootMotionViaAnimation.x = 0;

							// if we're in the air, we prevent the character from being pushed back.
							if (!State.IsGrounded && (Speed.y != 0))
							{
								_newPosition.x = 0;
							}
							_contactList.Add(_sideHitsStorage[i]);
							_speed.x = 0;
						}

						break;
					}
				}
			}
		}

		//speed not affected by root motion
		protected override void ComputeNewSpeed()
		{
			// we compute the new speed
			if (Time.deltaTime > 0)
			{
				_speed = (_newPosition - _rootMotionViaAnimation) / Time.deltaTime; //DON'T APPLY ROOT MOTION TO NEW SPEED
			}

			// we apply our slope speed factor based on the slope's angle
			if (State.IsGrounded)
			{
				_speed.x *= Parameters.SlopeAngleSpeedFactor.Evaluate(Mathf.Abs(State.BelowSlopeAngle) * Mathf.Sign(_speed.y));
			}

			if (!State.OnAMovingPlatform)
			{
				// we make sure the velocity doesn't exceed the MaxVelocity specified in the parameters
				_speed.x = Mathf.Clamp(_speed.x, -Parameters.MaxVelocity.x, Parameters.MaxVelocity.x);
				_speed.y = Mathf.Clamp(_speed.y, -Parameters.MaxVelocity.y, Parameters.MaxVelocity.y);
			}
		}

		//match visual go with root go
		void RepositionInternalGO()
		{
			if (internalGO.transform.localPosition != Vector3.zero)
			{
				_rootMotionViaAnimation = internalGO.transform.localPosition;
				internalGO.transform.Translate(internalGO.transform.localPosition * -1);
			}
		}
	}
}