﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
	public class ChildAttackHitbox : MonoBehaviour
	{
		public LevelManagerManablade manager;
		public Rigidbody rb;
		public CharacterStatus status;
		public Collider myCollider;
		public CharacterManablade myCharacter;
		
		//Runtime
		RuntimeAttackData currentAttack;

		public List<Collider> history;

		Vector2 contactPoint;
		bool currentlyEnabled;

		void Start()
		{
			rb = GetComponent<Rigidbody>();
			status = GetComponentInParent<CharacterStatus>();
			myCharacter = GetComponentInParent<CharacterManablade>();
			myCollider = GetComponent<Collider>();
			manager = (LevelManagerManablade)LevelManagerManablade.Instance;

			ClearCollisionHistory();
			currentlyEnabled = false;
			DisableHitbox();
		}
		bool IsInCollisionHistory(Collider col)
		{
			return history.Contains(col);
		}
		void AddToCollisionHistory(Collider col)
		{
			//Debug.Log(manager.time + ": attack " + currentAttack.id +" remembers " + col.gameObject.name);
			history.Add(col);
		}
		void ClearCollisionHistory()
		{
			//Debug.Log("clear " + currentAttack.id);
			history.Clear();
		}
		public void EnableHitbox(RuntimeAttackData attack)
		{
			//Debug.Log("enable " + transform.name + " " + attack.id);
			currentAttack = attack;

			myCollider.enabled = true;
			currentlyEnabled = true;
		}
		public void DisableHitbox()
		{
			if (!currentlyEnabled) return;
			//Debug.Log("disable " + transform.name);

			myCollider.enabled = false;
			currentlyEnabled = false;

			ClearCollisionHistory();
		}

		void OnTriggerEnter(Collider col)
		{
			//Debug.Log("collided with " + col.gameObject.name);

			if (!col.transform.root.gameObject.GetComponentInChildren<CharacterManablade>()) return; //wasn't an actor
			if (col.transform.root.gameObject.GetComponentInChildren<CharacterStatus>().isDead) return; //ignore dead bodies
			if (IsInCollisionHistory(col)) return; //already dealt with this collider
			//if (actor.IsAlreadyHitByAttack(currentAttack.id)) return; //???? why is this checking if i hit myself ???

			CharacterManablade victim = col.transform.root.gameObject.GetComponentInChildren<CharacterManablade>();
			bool isClash = col.GetComponent<TagHitbox>();
			bool isHit = col.GetComponent<TagHurtbox>();
			HurtboxLocation hurtbox = isHit ? col.GetComponent<TagHurtbox>().location : HurtboxLocation.nil;

			if (isClash || isHit)
			{
				AddToCollisionHistory(col);

				manager.AddCollisionEvent(new RuntimeCollisionData {
					attack = currentAttack,
					attacker = myCharacter,
					defender = victim,

					location = myCollider.bounds.ClosestPoint(col.transform.position),

					hurtbox = hurtbox,
					clashAttackId = col.GetComponent<ChildAttackHitbox>().currentAttack.id,
					isClash = isClash,
					isHit = isHit
				});
			}
		}
	}
}