﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using System;

namespace MoreMountains.CorgiEngine
{	
	[AddComponentMenu("Corgi Engine/Character/Abilities/Character Action Steps")]
	public class CharacterStatus : MonoBehaviour
	{
		public ChildAttackHitbox _hitbox;
		public Animator _animator;
		public CharacterMobility _mobility;

		public float stepTicker;
		public int currentStep;
		public ActionType currentAction;
		public DatabaseAction database;
		public bool canDefend, canAttack, canMove;
		public bool canLand;
		public bool upperBodyInvincibility, lowerBodyInvincibility;
		public bool isDead;
		


		void Start()
		{
			_animator = GetComponentInChildren<Animator>();
			_mobility = GetComponent<CharacterMobility>();
			_hitbox = GetComponentInChildren<ChildAttackHitbox>();
			database = DatabaseAction.Instance;
			InitializeValues();
			isDead = false;
		}

		protected void InitializeValues()
		{
			stepTicker = 0;
			currentStep = -1;
			canDefend = canAttack = canMove = true;
			canLand = true;
			upperBodyInvincibility = lowerBodyInvincibility = false;
			currentAction = ActionType.Neutral;
		}

		void Update()
		{
			if (currentAction == ActionType.Neutral || currentStep < 0) //no reason to tick
				return;

			HandleWalking();
			TickCounter();
			while(NeedToChangeStep())
				ProgressStep();
		}

		private void HandleWalking()
		{
			if (currentAction != ActionType.Walk) return;

			if (_mobility.GetHorizontalInput() < _mobility.InputThreshold)
			{
				StartAction(ActionType.Neutral);
			}
		}

		private void ProgressStep()
		{
			
			//Debug.Log("progressing " + currentStep + "/" + database.GetFrameDataList(currentAction).Count);

			currentStep += 1;
			//Debug.Log(Time.frameCount + " " + currentAction + ": " + database.GetFrameDataEvent(currentAction, currentStep).type);

			switch(database.GetFrameDataEvent(currentAction, currentStep).type)
			{
				case FrameDataType.startup:
				case FrameDataType.interimStartup:
					canAttack = false;
					canDefend = false;
					canMove = false;
					break;

				case FrameDataType.activeMelee:
				case FrameDataType.activeGrab:
				case FrameDataType.activeRanged:
					//_hitbox.EnableHitbox()
					canAttack = false;
					canDefend = false;
					canMove = false;
					break;

				case FrameDataType.recovery:
					//Debug.Log("recovery");
					canAttack = false;
					canDefend = false;
					canMove = false;
					break;

				case FrameDataType.cancellable:
					canAttack = true;
					canDefend = true;
					canMove = false;
					break;

				case FrameDataType.neutral:
					canAttack = true;
					canDefend = true;
					canMove = true;
					break;
			}

			if (currentStep >= database.GetFrameDataList(currentAction).Count - 1)
			{
				//Debug.Log(Time.frameCount + " finished");
				StartAction(ActionType.Neutral);
				//currentAction = ActionType.Idle;
				currentStep = -1;
			}
		}

		private bool NeedToChangeStep()
		{
			if (currentAction == ActionType.Neutral || currentStep < 0)
				return false;

			//Debug.Log("checking " + currentStep);

			//if (currentStep < database.GetFrameDataList(currentAction).Count &&
			//	stepTicker >= database.GetFrameDataEvent(currentAction, currentStep).timing)
				//Debug.Log("");

			if (stepTicker >= database.GetFrameDataEvent(currentAction, currentStep).timing)
				return true;
			else
				return false;
		}

		private void TickCounter()
		{
			stepTicker += Time.deltaTime;
		}
		public void StartAction(ActionType act)
		{
			//Debug.Log(Time.frameCount + " start action: " + act);
			_animator.Play(database.GetAnimationName(act), -1, 0);
			currentAction = act;
			
			stepTicker = 0;

			if (act == ActionType.Neutral || act == ActionType.Walk || act == ActionType.Jump)
			{
				canMove = true;
				canDefend = true;
				canAttack = true;
				currentStep = -1;
			}
			else
			{
				canMove = canDefend = canAttack = false;
				currentStep = 0;
			}
		}
	}
}