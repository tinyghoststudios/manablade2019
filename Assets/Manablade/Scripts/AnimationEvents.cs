﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
	public class AnimationEvents : MonoBehaviour
	{
		public CharacterVFX _vfx;

		public void Start()
		{
			_vfx = GetComponentInParent<CharacterVFX>();
		}

		public void PlayVFX(ActionType act)
		{
			Instantiate(_vfx._vfxGameObjects[0], _vfx.vfxSpawnLocation.transform.position, _vfx.vfxSpawnLocation.transform.rotation);
		}
	}
}