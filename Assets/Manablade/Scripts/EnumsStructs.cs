﻿using UnityEngine;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
	#region ENUMS
	public enum AttributeType
	{
		Strengh, Intelligence, Vitality, Agility, Dexterity, Luck
	}
	public enum ParameterType
	{
		Health, //100~1000
		Mana, //100~1000
		Endurance, 
		Burst,

		TremorResistance,
		ParalyzeResistance,
		FreezeResistance,
		BurnResistance,

		//Multipliers, default 1.0
		PhysicalDamageDealt, //STR
		PhysicalDamageTaken, //VIT
		MagicalDamageDealt, //INT
		MagicalDamageTaken, //INT
		KnockbackDealt, //STR
		KnockbackTaken, //VIT
		HitstunDealt, //STR
		HitstunTaken, //STR
		Invincibility, //DEX
		MovementSpeed, //AGI
		InstantBlock, //DEX
		CritialHitChance, //LUK
		CriticalHitDamage, //LUK

		FistDamage,
		SwordDamage,
		AxeDamage,
		LanceDamage,
	}
	public enum ActionType
	{
		Neutral = 1, Walk = 2, Crouch = 3, Dash = 4, Jump = 5, Land = 6, Guard = 7, Dodge = 8, Backflip = 9,
		Teleport = 10, Roll = 11,
		FistJab = 21, FistKick = 22, FistHook = 23,

		SwordMitsuru5B = 41, swordMitsuruDroit = 42
	}
	public enum FrameDataType
	{
		nil = 0,
		startup,
		interimStartup,
		activeMelee, activeRanged, activeGrab,
		recovery,
		cancellable,
		moveable,
		neutral,
		invincibility
	}
	public enum HurtboxLocation
	{
		nil, Head, Chest, Hip, Arm, Leg
	}
	public enum WeaponType
	{
		Nothing, Fist, Sword, Axe, Lance, Staff
	}
	public enum KnockbackType
	{
		nil = 0,
		normal = 1,
		sweep = 2,
		launch = 3,
		juggle = 4,
		bounce = 5,
		grab = 6
	}
	#endregion
	#region STRUCTS
	[System.Serializable]
	public struct RuntimeAttackData
	{
		public AttackEvent attack;
		public int id;
	}

	[System.Serializable]
	public struct RuntimeCollisionData
	{
		public RuntimeAttackData attack;
		public CharacterManablade attacker, defender;
		public Vector3 location;
		public HurtboxLocation hurtbox;
		public bool isClash, isHit;
		public int clashAttackId;
	}
	[System.Serializable]
	public struct FrameDataTimingEvent
	{
		public FrameDataType type; //what kind of step is this?
		public float timing; //at which frame should this be progressed to?
		public int index; //which attack # is this? which invincibility is this?
	}
	[System.Serializable]
	public struct AttackEvent //Information about each particular hit of an action
	{
		public int frameInterimStartup, frameActive, frameRecovery, frameCancellable;
		[Header("")]
		public WeaponType hitbox;
		//public HitsparkType hitsparkType;
		public int damage;
		public bool canTurn;
		public ActionType actionOnWhiff;

		[Header("")]
		[Range(1, 5)]
		public int level;

		[Header("")]
		public KnockbackType knockbackTypeGround;
		public Vector2 knockbackVectorGround;
		public KnockbackType knockbackTypeAir;
		public Vector2 knockbackVectorAir;
		public bool releasesGrabVictim;

		[Header("")]
		[Range(0, 1)]
		public float gravityScaleValue;
		public int gravityScaleDuration;

		[Header("")]
		public bool spawnsProjectile;
		//public ProjectileType projectileType;
	}
	[System.Serializable] //Information about each instance of invincibility during an action
	public struct InvincibilityEvent
	{
		public int activeFrame;
		public int duration;
		public bool upperbody, lowerbody;
	}
	#endregion
}