﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
	#region SCRIPTABLE OBJECTS
	[CreateAssetMenu(fileName = "NewAction", menuName = "Action Data")]

	public class ActionData : ScriptableObject
	{
		public DataAction[] actions;
	}

	[System.Serializable]
	public class DataAction
	{
		public ActionType action = ActionType.Neutral;
		public string animationName = "Idle";
		public WeaponType visibleWeapon = WeaponType.Nothing;

		[Header("Invincibiliy Events")]
		[Tooltip("List of invincibility triggers")]
		public InvincibilityEvent[] invincibility;

		[Header("Active Attack List")]
		[Tooltip("List of attacks for this action")]
		public AttackEvent[] attack;

		[Tooltip("Frame when the player can move cancel")]
		public int frameMoveable;
		[Tooltip("Frame when the player returns to neutral")]
		public int frameNeutral;

		[Header("Costs")]
		public int healthCost;
		public int manaCost;
		public int staminaCost;
		[Header("Special Properties")]
		[Tooltip("Can this be interrupted by landing on the ground?")]
		public bool canLand = true;
	}
	#endregion
}