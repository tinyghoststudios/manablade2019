﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
	public class DatabaseAction : MonoBehaviour
	{
		#region VARIABLES

		public ActionData actionDatabase;
		//public DataAction[] actionDatabase.actions;
		[HideInInspector] public int[] actionLookup;
		int maxActionArrayLength = 60;

		public List<FrameDataTimingEvent>[] frameData;
		#endregion
		#region INITIALIZATION
		void Start()
		{
			PopulateLookupTables();
			CreateFrameData();
		}
		void PopulateLookupTables() //hardcoded array size 
		{
			actionLookup = new int[maxActionArrayLength];
			for (int i = 0; i < actionDatabase.actions.Length; i++)
			{
				actionLookup[(int)actionDatabase.actions[i].action] = i;
			}
		}
		void CreateFrameData()
		{
			//Debug.Log("Creating Frame Data");
			frameData = new List<FrameDataTimingEvent>[actionDatabase.actions.Length];
			for (int act = 0; act < frameData.Length; act++)
			{
				//Debug.Log("action " + actionDatabase.actions[act]);
				frameData[act] = new List<FrameDataTimingEvent>();

				frameData[act].Add(new FrameDataTimingEvent //startup
				{
					timing = 0f,
					type = FrameDataType.startup
				});

				//Add interim startup, active, recovery and cancelable frames
				for (int att = 0; att < actionDatabase.actions[act].attack.Length; att++)
				{
					frameData[act].Add(new FrameDataTimingEvent //recovery
					{
						timing = actionDatabase.actions[act].attack[att].frameInterimStartup / 60f,
						type = FrameDataType.interimStartup,
						index = att
					});

					FrameDataType activeType;
					if (actionDatabase.actions[act].attack[att].spawnsProjectile)
						activeType = FrameDataType.activeRanged;
					else if (actionDatabase.actions[act].attack[att].knockbackTypeGround == KnockbackType.grab)
						activeType = FrameDataType.activeGrab;
					else
						activeType = FrameDataType.activeMelee;
					
					frameData[act].Add(new FrameDataTimingEvent //active
					{
						timing = actionDatabase.actions[act].attack[att].frameActive / 60f,
						type = activeType,
						index = att
					});

					frameData[act].Add(new FrameDataTimingEvent //recovery
					{
						timing = actionDatabase.actions[act].attack[att].frameRecovery / 60f,
						type = FrameDataType.recovery,
						index = att
					});
					frameData[act].Add(new FrameDataTimingEvent //cancelable
					{
						timing = actionDatabase.actions[act].attack[att].frameCancellable / 60f,
						type = FrameDataType.cancellable,
						index = att
					});
				}

				float pendingTime;

				//Add invincibility events
				for (int inv = 0; inv < actionDatabase.actions[act].invincibility.Length; inv++)
				{
					pendingTime = actionDatabase.actions[act].invincibility[inv].activeFrame / 60f;

					for (int i = 0; i < frameData[act].Count; i++) //find insertion point
					{
						if (frameData[act][i].timing >= pendingTime)
						{
							frameData[act].Insert(i, new FrameDataTimingEvent //invincibility
							{
								timing = pendingTime,
								type = FrameDataType.invincibility,
								index = inv
							});

							break;
						}
					}
				}

				//Add remaining move and neutral events
				frameData[act].Add(new FrameDataTimingEvent
				{
					timing = actionDatabase.actions[act].frameMoveable / 60f,
					type = FrameDataType.moveable
				});
				frameData[act].Add(new FrameDataTimingEvent
				{
					timing = actionDatabase.actions[act].frameNeutral / 60f,
					type = FrameDataType.neutral
				});

				//Debug.Log(actionDatabase.actions[act].action + " frame data");
				//for (int d = 0; d < frameData[act].Count; d++)
				//	Debug.Log(d + ": " + frameData[act][d].type + " " + frameData[act][d].timing);
			}
		}
		#endregion
		#region GET
		//Action Data
		public float GetHealthCost(ActionType state)
		{
			return actionDatabase.actions[actionLookup[(int)state]].healthCost;
		}
		public string GetAnimationName(ActionType state)
		{
			return actionDatabase.actions[actionLookup[(int)state]].animationName;
		}

		//Frame Data
		public List<FrameDataTimingEvent> GetFrameDataList(ActionType state)
		{
			return frameData[actionLookup[(int)state]];
		}
		public FrameDataTimingEvent GetFrameDataEvent(ActionType state, int stage)
		{
			return frameData[actionLookup[(int)state]][stage];
		}
		#endregion

		#region Singleton
		private static DatabaseAction instance;
		void Awake()
		{
			instance = this;
		}
		public static DatabaseAction Instance
		{
			get { return instance; }
		}
		
		#endregion
	}
}