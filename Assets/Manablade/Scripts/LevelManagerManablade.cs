﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.Tools;
using MoreMountains.MMInterface;

namespace MoreMountains.CorgiEngine
{
	/// <summary>
	/// Spawns the player, handles checkpoints and respawn
	/// </summary>
	[AddComponentMenu("Corgi Engine/Managers/Level Manager MB")]
	public class LevelManagerManablade : LevelManager
	{
		#region GENERAL
		public List<Character> actor; //list of actors in the scene

		public override void Start()
		{
			base.Start();
			//Debug.Log(Time.frameCount + "Battle Manager Init");
			Application.targetFrameRate = 60;
			QualitySettings.vSyncCount = 0;
			QualitySettings.maxQueuedFrames = 0;

			InitializeCollisionLists();
		}
		protected override void InstantiatePlayableCharacters()
		{
			//Debug.Log("instantiating playable chars");
			Players = new List<Character>();

			// we check if there's a stored character in the game manager we should instantiate
			if (GameManager.Instance.StoredCharacter != null)
			{
				Character newPlayer = (Character)Instantiate(GameManager.Instance.StoredCharacter, new Vector3(0, 0, 0), Quaternion.identity);
				newPlayer.name = GameManager.Instance.StoredCharacter.name;
				Players.Add(newPlayer);
				return;
			}

			//LAZILY ADD PLAYER OBJS IN THE EDITOR
			Character[] sceneCharacters = FindObjectsOfType<Character>();

			foreach (Character cha in sceneCharacters)
			{
				//Character newPlayer = (Character)Instantiate(cha, new Vector3(0, 0, 0), Quaternion.identity);
				//newPlayer.name = cha.name;
				//Debug.Log("adding " + cha.name);
				Players.Add(cha);
			}

			if (PlayerPrefabs == null) { return; }
			// player instantiation
			if (PlayerPrefabs.Count() != 0)
			{
				foreach (Character playerPrefab in PlayerPrefabs)
				{
					Character newPlayer = (Character)Instantiate(playerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
					newPlayer.name = playerPrefab.name;
					Players.Add(newPlayer);

					if (playerPrefab.CharacterType != Character.CharacterTypes.Player)
					{
						Debug.LogWarning("LevelManager : The Character you've set in the LevelManager isn't a Player, which means it's probably not going to move. You can change that in the Character component of your prefab.");
					}
				}
			}
			else
			{
				//Debug.LogWarning ("LevelManager : The Level Manager doesn't have any Player prefab to spawn. You need to select a Player prefab from its inspector.");
				return;
			}
		}

		#endregion
		#region COLLISIONS
		public List<RuntimeCollisionData> hit; //list of collisions
		//public CollisionEvent clash, secondHitInTrade;

		void InitializeCollisionLists()
		{
			hit = new List<RuntimeCollisionData>();
			actor = new List<Character>();
		}

		public void AddCollisionEvent(RuntimeCollisionData ev)
		{
			hit.Add(ev);
		}

		#endregion
	}
}
