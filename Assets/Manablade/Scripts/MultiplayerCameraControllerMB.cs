﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using System.Collections.Generic;

namespace MoreMountains.CorgiEngine
{
	public class MultiplayerCameraControllerMB : MultiplayerCameraController
	{
		public float ZoomInDampTime = .4f;
		float requiredSize; //let other functions compare dest vs curr ortho size; for cameramovement

		public float ExtraVerticalPadding = .2f;

		//UPDATE INSIDE LATEUPDATE INSTEAD OF FIXED
		protected override void FixedUpdate() { }
		protected virtual void LateUpdate()
		{
			CameraMovement();
		}

		//APPLY MY OWN BUFFER
		protected override void CameraMovement()
		{
			// We need a list of targets to follow
			if (Players == null)
				return;

			CleanPlayersList();
			FindAveragePosition();
			ComputeZoom();
			ClampNewPosition();
			FindBetterPosition();
			MoveCamera();
		}
		protected void FindBetterPosition()
		{
			float tempY = _newPosition.y - requiredSize;
			float goodHeight = requiredSize + ExtraVerticalPadding / 2;
			float goodY = tempY + goodHeight;
			
			_newPosition = new Vector3(_newPosition.x, goodY, _newPosition.z);
		}
		//USE A DIFFERENT DAMP SPEED FOR ZOOMING IN
		protected override void ComputeZoom()
		{
			if (_camera.orthographic)
			{
				//float requiredSize;
				// Find the required size based on the desired position and smoothly transition to that size.
				requiredSize = FindRequiredOrthographicSize();
				_camera.orthographicSize = Mathf.SmoothDamp(
					_camera.orthographicSize, 
					requiredSize, 
					ref _zoomSpeed, 
					_camera.orthographicSize < requiredSize ? DampTime : ZoomInDampTime);
				GetLevelBounds();
			}
			else
			{
				float requiredDistance;
				requiredDistance = FindRequiredDistance();
				_newPosition.z = -requiredDistance;
			}
		}
		protected override void MoveCamera()
		{
			// Smoothly transition to that position.
			transform.position = Vector3.SmoothDamp(
				transform.position, 
				_newPosition, 
				ref _moveVelocity,
				_camera.orthographicSize < requiredSize ? DampTime : ZoomInDampTime);
		}

		//ACCOUNT FOR HEIGHTS
		protected override void FindAveragePosition()
		{
			_averagePosition = Vector3.zero;
			int numTargets = 0;

			//Debug.Log("number of targts " + Players.Count);
			// Go through all the targets and add their positions together.
			for (int i = 0; i < Players.Count; i++)
			{
				// Add to the average and increment the number of targets in the average.
				Vector3 tempPos = Players[i].position;
				//ADD HEIGHTS
				tempPos += new Vector3(0, Players[i].GetComponent<BoxCollider2D>().size.y / 2, 0); 
				_averagePosition += tempPos;
				numTargets++;
			}

			// If there are targets divide the sum of the positions by the number of them to find the average.
			if (numTargets > 0)
				_averagePosition /= numTargets;

			// we fix the z value
			_averagePosition.z = _initialZ;

			// The desired position is the average position;
			_newPosition = _averagePosition;
		}

		//ACCOUNT FOR HEIGHTS
		protected override float FindRequiredOrthographicSize()
		{
			Vector3 desiredLocalPos = transform.InverseTransformPoint(_newPosition);

			float size = 0f;

			for (int i = 0; i < Players.Count; i++)
			{
				Vector3 targetLocalPos = transform.InverseTransformPoint(Players[i].position);
				float tempHeight = Players[i].GetComponent<BoxCollider2D>().size.y;
				targetLocalPos += new Vector3(0, tempHeight, 0);

				Vector3 desiredPosToTarget = targetLocalPos - desiredLocalPos;

				size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.y));
				size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.x) / _camera.aspect);
			}
			//Debug.Log("");Debug.Log("");
			//Debug.Log("size without buffer " + size);
			size += OrthoScreenEdgeBuffer + ExtraVerticalPadding / 2;
			//Debug.Log("size with buffer " + size);


			size = Mathf.Max(size, OrthoMinSize);

			return size;
		}
	}
}